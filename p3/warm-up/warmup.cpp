#include <vector>
using std::vector;
#include <iostream>
using std::cout;

vector<vector<bool> > world = {
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, /* world[0] */
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, /* world[1] */
  // ^ (world[1])[2]
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, /* ... */
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
};

int main(int argc, char *argv[])
{
	vector<vector<bool>> future(world); /* make future a copy of world. */
	/* NOTE: the vector 'world' above corresponds to the contents
	 * of ../res/tests/0.
	 * TODO: before moving on, make sure you can correctly apply the rules to
	 * the vector ONCE.  Write your result to standard output, and compare with
	 * the contents of ../tests/1 to check.
	 * Once that is working, read argv[0] to get an integer n and then apply the
	 * rules n times before printing. */
	world = future;
	return 0;
}
